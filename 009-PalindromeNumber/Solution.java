class Solution {
    public boolean isPalindrome(int number) {
		int reverted = 0;

		if (number < 0 || ((number % 10 == 0) && number != 0)) return false;

		while (number > reverted) {
			reverted = reverted * 10 + number % 10;
			number /= 10;
		}


		return reverted == number || (reverted/10) == number;
    }
}