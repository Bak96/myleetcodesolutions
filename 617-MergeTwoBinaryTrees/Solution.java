class Solution {
	public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
		TreeNode newTree = new TreeNode(0);

		return mergeRecursive(newTree, t1, t2);
	}

	private TreeNode mergeRecursive(TreeNode builder, TreeNode t1, TreeNode t2) {
		if (t1 == null && t2 == null) {
			return null;
		}

		int t1Val = 0;
		TreeNode t1Left = null;
		TreeNode t1Right = null;

		int t2Val = 0;
		TreeNode t2Left = null;
		TreeNode t2Right = null;

		if (t1 != null) {
			t1Val = t1.val;
			t1Left = t1.left;
			t1Right = t1.right;
		}

		if (t2 != null) {
			t2Val = t2.val;
			t2Left = t2.left;
			t2Right = t2.right;
		}

		builder.val = t1Val + t2Val;
		builder.left = new TreeNode(0);
		builder.right = new TreeNode(0);

		builder.left = mergeRecursive(builder.left, t1Left, t2Left);
		builder.right = mergeRecursive(builder.right, t1Right, t2Right);

		return builder;
	}
}