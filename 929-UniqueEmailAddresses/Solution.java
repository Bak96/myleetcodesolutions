import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Solution {
    public int numUniqueEmails(String[] emails) {
        Set<String> uniqueEmails = new HashSet<>();

        Arrays.stream(emails).forEach(
                email -> uniqueEmails.add(parseEmail(email))
        );

        return uniqueEmails.size();        
    }
    
    
    
    public static String parseEmail(String email) {
        String localName = email.substring(0, email.indexOf('@'));
        String domainName = email.substring(email.indexOf('@'), email.length());
        int plusSign = localName.indexOf('+');

        if (plusSign >= 0) {
            localName = localName.substring(0, plusSign);
        }

        localName = localName.replace(".", "");

        return localName + domainName;
    }
}