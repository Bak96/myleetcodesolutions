class Solution {
	public int[][] flipAndInvertImage(int[][] A) {
		for (int i = 0; i < A.length; i++) {
			flipAndInvertImageRow(A[i]);
		}

		return A;
	}

	public void flipAndInvertImageRow(int[] row) {
		for (int i = 0; i < row.length / 2; i++) {
			int tmp = row[i];

			int mirrorIndex = row.length - i - 1;
			row[i] = row[mirrorIndex] ^ 0x1;
			row[mirrorIndex] = tmp ^ 0x1;
		}

		if (row.length % 2 != 0) {
			row[row.length / 2] ^= 0x1;
		}
	}
}