class Solution {
    public String longestPalindrome(String s) {
        int maxLength = 0;
        int mid = 0;
        
        if (s.isEmpty()) {
            return "";
        }
        
        for (int i = 0; i < s.length(); i++) {
            int first = expand(s, i, i);
            int second = expand(s, i, i+1);
            
            int length = Math.max(first, second);
            
            if (length > maxLength) {
                maxLength = length;
                mid = i;
            }
        }
        
        if (maxLength % 2 == 0) { // hack for following subtraction
            mid++;
        }
        
        return longestPalindrome(s, mid - maxLength / 2, maxLength);
    }   
    
    public int expand(String s, int left, int right) {
        while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            left--;
            right++;
        }
        
        left++;
        right--;
        
        return right - left + 1;
    }
    
    public String longestPalindrome(String s, int start, int length) {
        return s.substring(start, start + length);
    }
}