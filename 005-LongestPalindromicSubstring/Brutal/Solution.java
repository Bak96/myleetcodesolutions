class Solution {
    public String longestPalindrome(String s) {
        if (s.isEmpty()) {
            return "";
        }
        
        int longestPalindromeStart = 0;
        int longestPalindromeLength = 1;
        boolean lastFound = true;
        for (int length_to_check = 2; length_to_check <= s.length(); length_to_check++) {
            for (int j = 0; j <= s.length() - length_to_check; j++) {
                if(isPalindrome(s, j, length_to_check)) {
                    longestPalindromeStart = j;
                    longestPalindromeLength = length_to_check;
                    lastFound = true;
                    break;
                }
            }
            
            if (longestPalindromeLength != length_to_check && lastFound == false) {
                break;
            }
            
            if (longestPalindromeLength != length_to_check) {
                lastFound = false;
            }
        }
        
        return longestPalindrome(s, longestPalindromeStart, longestPalindromeLength);
    }
    
    public boolean isPalindrome(String s, int start, int length) {
 
        int end = start + length - 1;
        for (int i = start; i < start + (length / 2); i++, end--) {
            if (s.charAt(i) != s.charAt(end)) {
                return false;
            }
            
        }
        return true;
    }
    
    public String longestPalindrome(String s, int start, int length) {
        return s.substring(start, start + length);
    }
}