import javafx.util.Pair;

import java.util.HashSet;
import java.util.Set;


class Solution {
    public int robotSim(int[] commands, int[][] obstacles) {
        Set<Pair> obstacleSet = new HashSet<>();
        Pair<Integer, Integer> position = new Pair<>(0, 0);

        int max = 0;

        for (int i = 0; i < obstacles.length; i++) {
            obstacleSet.add(new Pair<>(obstacles[i][0], obstacles[i][1]));
        }

        int direction = 0; // 0 - north, 1 - east, 2 - south, 3 - west
        for (int i = 0; i < commands.length; i++) {
            if (commands[i] == -2) { //turn left
                direction -= 1;
                direction = direction < 0 ? 3 : direction;

            } else if (commands[i] == -1) { //turn right
                direction = (direction + 1) % 4;
            } else { //move forward
                position = moveForward(position, commands[i], direction, obstacleSet);
                max = Math.max(max, calculateDistance(position));
            }
        }

        return max;
    }
    
    private int calculateDistance(Pair<Integer, Integer> position) {
        return position.getKey() * position.getKey() + position.getValue() * position.getValue();
    }
    
    private Pair<Integer, Integer> moveForward(Pair<Integer, Integer> position, int steps, int direction, Set<Pair> obstacleSet) {
        boolean blocked = false;

        int actX = position.getKey();
        int actY = position.getValue();

        for (int i = 0; i < steps && !blocked; i++) {
            int newX = actX;
            int newY = actY;

            switch (direction) {
                case 0:
                    newY++;
                    break;
                case 1:
                    newX++;
                    break;
                case 2:
                    newY--;
                    break;
                case 3:
                    newX--;
                    break;
            }

            if (obstacleSet.contains(new Pair<>(newX, newY))) {
                blocked = true;
            }
            else {
                actX = newX;
                actY = newY;
            }
        }

        return new Pair<>(actX, actY);
    }
}