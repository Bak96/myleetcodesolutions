import java.util.Stack;

public class Test {
	public static void main(String[] args) {

	}

	public int maxArea(int[] height) {
		int i = 0;
		int top = -1;
		int right;
		int area;
		int maxArea = 0;
		Stack<Integer> stack = new Stack<Integer>();

		while (i < height.length) {
			//wkladanie
			while(i < height.length && top <= height[i]) {
				stack.push(i);
				top = height[i];
				i++;
			}

			if (i < height.length) right = height[i];
			else right = 0;

			//zdejmowanie
			while (!stack.isEmpty() && top > right) {
				stack.pop();

				if (!stack.isEmpty()){
					area = top*(i - 1 - stack.peek());
					top = height[stack.peek()];
				}
				else {
					area = top*(i - 1 - 0);
					top = 0;
				}

				if (area > maxArea) maxArea = area;
			}
		}

		return maxArea;
	}
}
