class Solution {
    public int maxArea(int[] height) {
		int left = 0;
		int right = height.length - 1;
		int area;
		int maxArea = 0;

		while (left < right) {
			int min = Math.min(height[left], height[right]);
			area = min*(right - left);

			if (area > maxArea) maxArea = area;

			if (height[right] < height[left]) right--;
			else left++;
		}

		return maxArea;
    }
}
