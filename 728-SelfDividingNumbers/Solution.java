import java.util.ArrayList;
import java.util.List;

class Solution {
    public List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> list = new ArrayList<>(right - left + 1);

        for (int i = left; i <= right; i++) {
            if (selfDividingNumber(i)) {
                list.add(i);
            }
        }

        return list;
    }

    public boolean selfDividingNumber(int number) {
        int tmp = number;

        while (tmp > 0) {
            int digit = tmp % 10;
            if (digit == 0 || number % digit != 0) {
                return false;
            }

            tmp /= 10;
        }

        return true;
    }
}