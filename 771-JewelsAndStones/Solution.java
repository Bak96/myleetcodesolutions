class Solution {
    public int numJewelsInStones(String J, String S) {
        int[] counter = new int[256];

        for (int i = 0; i < S.length(); i++) {
            counter[S.charAt(i)]++;
        }

        int jewels = 0;
        for (int i = 0; i < J.length(); i++) {
            jewels += counter[J.charAt(i)];
        }

        return jewels;
    }
}