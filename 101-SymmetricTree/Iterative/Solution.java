class Solution {
	public static boolean isSymmetric(TreeNode root) {
        if (root == null){
            return true;
        }
        
		Queue<TreeNode> queue = new LinkedList<TreeNode>();

		queue.add(root.left);
		queue.add(root.right);

		while (!queue.isEmpty()) {
			TreeNode t1 = queue.poll();
			TreeNode t2 = queue.poll();

			if (t1 == null && t2 == null) {
				continue;
			}
			else if (t1 == null || t2 == null) {
				return false;
			}

			if (t1.val != t2.val) {
				return false;
			}

			queue.add(t1.left);
			queue.add(t2.right);
			queue.add(t1.right);
			queue.add(t2.left);
		}

		return true;
	}
}