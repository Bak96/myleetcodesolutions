import java.util.Arrays;

class Solution {
    public int search(int[] nums, int target) {
        int stairs = findStairs(nums, target);
        int result;
        if (stairs > 0) {
            result = Arrays.binarySearch(nums, 0, stairs, target);
            if (result < 0) {
                result = Arrays.binarySearch(nums, stairs, nums.length, target);
            }
        }
        else {
            result = Arrays.binarySearch(nums, 0, nums.length, target);
        }
        
        if (result < 0) {
            return -1;
        }
        
        return result;
    }
    
    public int findStairs(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        int mid;
        
        while (left < right) {
            mid = (left + right) / 2;
            if (nums[mid] > nums[right]) { // musze szukac z prawej --> jesli jest mniejszy niz prawy to nic nie wiem
                left = mid+1;
            }
            else if (nums[mid] < nums[left]){ // musze szukac z lewej --> jesli jest wiekszy niz lewy to nic nie wiem
                right = mid;
            }
            else { //nie ma uskoku
                return left; //
            }
        }
                     
        return left;
    }
}