# Write your MySQL query statement below
update salary
    set salary.sex = case sex
                    when 'm' then 'f'
                    when 'f' then 'm'
                    else sex
                    end