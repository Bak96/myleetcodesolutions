class Solution {
    public int lengthOfLongestSubstring(String s) {
        boolean[] T = new boolean[256];
        int max = 0;
        int start = 0;
        int end = 0;
        
        if (s == null) return 0;
        
        while(end != s.length()) {
            if (!T[s.charAt(end)]) {
                T[s.charAt(end)] = true;
                
                int length = end - start + 1;
                if (length > max) max = length;
                
                end++;
            }
            else {
                while(s.charAt(start) != s.charAt(end)) {
                    T[s.charAt(start)] = false;
                    start++;
                }
                
                if (start != end) {
                    start++;
                    end++;
                }
            }
        }
        
        return max;
    }
}