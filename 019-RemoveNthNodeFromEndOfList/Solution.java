/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        int a = remove(head, n);
        
        if (a == n) return head.next;
        else return head;
    }
    
    public int remove(ListNode node, int n) {
        if (node == null) return 0;
        
        int x = remove(node.next, n);
        
        if (x == n) {
            if (n == 1) {
                node.next = null;
            }
            else {
                node.next = node.next.next;
            }
        }
        
        return x + 1;
    }
}