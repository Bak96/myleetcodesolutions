import java.util.Arrays;

class Solution {
	public int arrayPairSum(int[] nums) {
		int sum = 0;
		int n = nums.length / 2;

		Arrays.sort(nums);

		for (int i = 0; i < nums.length; i += 2) {
			sum += nums[i];
		}

		return sum;
	}
}
