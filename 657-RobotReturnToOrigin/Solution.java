class Solution {
	public boolean judgeCircle(String moves) {
		int upCounter = 0;
		int downCounter = 0;
		int leftCounter = 0;
		int rightCounter = 0;


		for (int i = 0; i < moves.length(); i++) {
			switch(moves.charAt(i)) {
				case 'U':
					upCounter++;
					break;
				case 'D':
					downCounter++;
					break;
				case 'L':
					leftCounter++;
					break;
				case 'R':
					rightCounter++;
					break;
				default:
					System.exit(1);
			}
		}

		return upCounter == downCounter && leftCounter == rightCounter;
	}
}
