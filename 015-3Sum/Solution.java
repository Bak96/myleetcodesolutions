import java.util.*;

class Solution {
	public List<List<Integer>> threeSum(int[] nums) {
		Map<Integer, Integer> counter = getCounterHashMap(nums);
		List<List<Integer>> resultList = new LinkedList<>();

		Arrays.sort(nums);

		int left = 0;
		int right;

		while (left < nums.length) {
			while (left < nums.length - 1 && nums[left] == nums[left + 1]) {
				left++;
			}

			int numberOfLeftValues = counter.get(nums[left]);

			if (numberOfLeftValues >= 3 && nums[left] == 0) { //only zero can be here, check left+left+left
				resultList.add(Arrays.asList(0, 0 ,0));
			} else if (numberOfLeftValues >= 2 && nums[left] != 0 && counter.containsKey(-2 * nums[left])) { //check left+left+valToZero
				resultList.add(Arrays.asList(nums[left], nums[left], -2 * nums[left]));
			}

			//check all left + right + valToZero, all different
			right = left + 1;
			while (right < nums.length) { //left, right - two different values
				while (right < nums.length - 1 && nums[right] == nums[right + 1]) {
					right++;
				}

				int valToZero = -(nums[left] + nums[right]);

				if (valToZero > nums[left] && valToZero > nums[right] && //must be different and greater to not duplicate anything from previous checks
						counter.containsKey(valToZero) && counter.get(valToZero) > 0) {
					resultList.add(Arrays.asList(nums[left], nums[right], valToZero));
				}

				right++;
			}

			left++;
		}

		return resultList;
	}

	private HashMap<Integer, Integer> getCounterHashMap(int[] nums) {
		HashMap<Integer, Integer> counter = new HashMap<>();

		for (int num : nums) {
			if (!counter.containsKey(num)) {
				counter.put(num, 1);
			} else {
				counter.put(num, counter.get(num) + 1);
			}
		}

		return counter;
	}
}