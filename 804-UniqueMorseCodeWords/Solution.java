class Solution {
    
    private static String[] morseCodes = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
    
    public int uniqueMorseRepresentations(String[] words) {
        Set<String> uniqueRepresentations = new HashSet<>();

        for (int i = 0; i < words.length; i++) {
            uniqueRepresentations.add(getMorseRepresentationOfWord(words[i]));
        }

        return uniqueRepresentations.size();
    }
    
    private static String getMorseRepresentationOfWord(String word) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < word.length(); i++) {
            builder.append(morseCodes[word.charAt(i) - 'a']);
        }

        return builder.toString();
    }
}