class Solution {
	public int[] sortArrayByParityII(int[] A) {
		//go through odd indexes and if there is even value move it to first free even index
		int nextFreeIndex = getFirstEvenIndexWithNotEvenValue(A, 0);

		for (int i = 1; i < A.length; i += 2) {
			if (A[i] % 2 == 0) {
				int tmp = A[i];
				A[i] = A[nextFreeIndex];
				A[nextFreeIndex] = tmp;
				nextFreeIndex = getFirstEvenIndexWithNotEvenValue(A, nextFreeIndex);
			}
		}

		return A;
	}

	private int getFirstEvenIndexWithNotEvenValue(int[] A, int startingFromIndex) {
		if (startingFromIndex % 2 != 0) {
			startingFromIndex++;
		}

		for (int i = startingFromIndex; i < A.length; i+=2) {
			if (A[i] % 2 != 0) {
				return i;
			}
		}

		return A.length;
	}
}