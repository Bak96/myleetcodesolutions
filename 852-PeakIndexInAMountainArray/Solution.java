class Solution {
    public int peakIndexInMountainArray(int[] A) {
        int index = 0;
        int left = 0;
        int right = A.length;

        while (left < right) {
            index = (left + right) / 2;

            if (index == 0) {
                return A[index] > A[index + 1] ? 0 : 1;
            } else if (index == A.length - 1) {
                return A[index] > A[index - 1] ? index: index - 1;
            }


            if (A[index - 1] < A[index] && A[index] > A[index + 1]) { //index jest albo najwyzszy, albo wyzszego ma z prawej
                return index;
            } else if (A[index - 1] < A[index]) {
                left = index + 1;
            } else {
                right = index;
            }
        }

        return index;
    }
}