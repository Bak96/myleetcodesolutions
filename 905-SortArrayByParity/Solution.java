class Solution {
    public int[] sortArrayByParity(int[] A) {
        int start = 0;
        int end = A.length - 1;
        
        while (start < end) {
            if (A[start] % 2 != 0) { //swap
                int temp = A[start];
                A[start] = A[end];
                A[end] = temp;
            
                end--;
            } else {
                start++;
            }   
        }
        
        return A;
    }
}