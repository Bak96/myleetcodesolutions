/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
      //  l1 = reverse(l1);
       // l2 = reverse(l2);
        
        ListNode l1_start = l1;
        ListNode l2_start = l2;
        
        int val;
        int p = 0;
        
        ListNode sum = new ListNode(0);
        ListNode sum_ptr = sum;
        while(l1 != null || l2 != null) {
            int val1;
            int val2;
            
            if (l1 == null) {
                val1 = 0;
            }
            else {
                val1= l1.val;
            }
            
            if (l2 == null) {
                val2 = 0;
            }
            else {
                val2 = l2.val;
            }
            
            
            
            val = (val1 + val2 + p) % 10;
            p = (val1 + val2 + p) / 10;
            
            sum_ptr.next = new ListNode(val);
            sum_ptr = sum_ptr.next;
            
            if (l1 != null) {
                l1 = l1.next;
            }
            
            if (l2 != null) {
                l2 = l2.next;
            }
            
        }
        
        if (p != 0) {
            sum_ptr.next = new ListNode(p);
        }
        
        return sum.next;
    }
    
    /*
    private ListNode reverse(ListNode node) {
        ListNode a = node.next;
        ListNode b;
        
        node.next = null;
        
        while (a != null) {
            b = a.next;
            a.next = node;
            node = a;
            a = b;
        }
        
        return node;
    }*/
}